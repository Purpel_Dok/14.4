﻿#include <iostream>
#include <string>

int main()
{
    setlocale(LC_ALL, "RU");

    std::cout << "Введите имя: ";
    std::string name;
    std::getline(std::cin, name);

    std::cout << "ВВедите возраста: ";
    std::string age;
    std::getline(std::cin, age);

    std::cout << "Место рождения: ";
    std::string fromlocale;
    std::getline(std::cin, fromlocale);
    


    std::cout << name << " \n" << age << " \n" << fromlocale << " \n";
   
    std::cout << name.length() << std::endl;
    std::cout << age.length() << std::endl;
    std::cout << fromlocale.length() << std::endl;
    
    std::cout << name[0];
    std::cout << name[name.length() - 1] << std::endl;
    
    std::cout << age[0];
    std::cout << age[age.length() - 1] << std::endl;
   
    std::cout << fromlocale[0];
    std::cout << fromlocale[fromlocale.length() - 1] << std::endl;

    std::cin;
    return 0;
}

